package ru.tsc.denisturovsky.tm.api.service;


import ru.tsc.denisturovsky.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    void remove(Project project);

    Project create(String name);

    Project create(String name, String description);

    Project add(Project project);

    void clear();

}
