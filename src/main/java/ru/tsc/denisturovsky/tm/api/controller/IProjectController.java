package ru.tsc.denisturovsky.tm.api.controller;

public interface IProjectController {

    void showProjectList();

    void clearProjects();

    void createProject();

}
