package ru.tsc.denisturovsky.tm.constant;

public final class TerminalCommand {

    public final static String ABOUT = "about";

    public final static String EXIT = "exit";

    public final static String HELP = "help";

    public final static String INFO = "info";

    public final static String VERSION = "version";

    public final static String WELCOME = "** WELCOME TO TASK MANAGER **";

    public final static String FIRST_NAME = "Denis";

    public final static String LAST_NAME = "Turovsky";

    public final static String EMAIL = "dturovsky@t1-consulting.ru";

    public final static String ARGUMENTS = "arguments";

    public final static String COMMANDS = "commands";

    public final static String TASK_CREATE = "task-create";

    public final static String TASK_LIST = "task-list";

    public final static String TASK_CLEAR = "task-clear";

    public final static String PROJECT_CREATE = "project-create";

    public final static String PROJECT_LIST = "project-list";

    public final static String PROJECT_CLEAR = "project-clear";

}
