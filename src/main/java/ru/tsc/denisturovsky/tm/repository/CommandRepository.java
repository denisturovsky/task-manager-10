package ru.tsc.denisturovsky.tm.repository;

import ru.tsc.denisturovsky.tm.api.repository.ICommandRepository;
import ru.tsc.denisturovsky.tm.constant.TerminalArgument;
import ru.tsc.denisturovsky.tm.constant.TerminalCommand;
import ru.tsc.denisturovsky.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static Command ABOUT = new Command(
            TerminalCommand.ABOUT, TerminalArgument.ABOUT,
            "Show developer info"
    );

    public static Command INFO = new Command(
            TerminalCommand.INFO, TerminalArgument.INFO,
            "Show system info"
    );

    public static Command HELP = new Command(
            TerminalCommand.HELP, TerminalArgument.HELP,
            "Show terminal commands list"
    );

    public static Command VERSION = new Command(
            TerminalCommand.VERSION, TerminalArgument.VERSION,
            "Show program version"
    );

    public static Command EXIT = new Command(
            TerminalCommand.EXIT, null,
            "Close application"
    );

    public static Command COMMANDS = new Command(
            TerminalCommand.COMMANDS, TerminalArgument.COMMANDS,
            "Show command list"
    );

    public static Command ARGUMENTS = new Command(
            TerminalCommand.ARGUMENTS, TerminalArgument.ARGUMENTS,
            "Show argument list"
    );

    public static Command TASK_CREATE = new Command(
            TerminalCommand.TASK_CREATE, null,
            "Create new task"
    );

    public static Command TASK_LIST = new Command(
            TerminalCommand.TASK_LIST, null,
            "Show task list"
    );

    public static Command TASK_CLEAR = new Command(
            TerminalCommand.TASK_CLEAR, null,
            "Remove all tasks"
    );

    public static Command PROJECT_CREATE = new Command(
            TerminalCommand.PROJECT_CREATE, null,
            "Create new project"
    );

    public static Command PROJECT_LIST = new Command(
            TerminalCommand.PROJECT_LIST, null,
            "Show project list"
    );

    public static Command PROJECT_CLEAR = new Command(
            TerminalCommand.PROJECT_CLEAR, null,
            "Remove all projects"
    );

    private final Command[] terminalCommands = new Command[]{
            ABOUT, INFO, HELP, VERSION,
            COMMANDS, ARGUMENTS,
            TASK_CLEAR, TASK_CREATE, TASK_LIST,
            PROJECT_CLEAR, PROJECT_CREATE, PROJECT_LIST,
            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return terminalCommands;
    }

}
